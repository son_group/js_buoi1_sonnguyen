function calculateSalary() {
    var averageSalaryEl = document.getElementById('txt-luong-ngay');
    var worknumberEl = document.getElementById('txt-day-number');
   
    var summary = averageSalaryEl.value*worknumberEl.value;
    document.getElementById('tong-luong').innerHTML = summary;
  
}

function calculateAverage5Number(){
    var num1El = document.getElementById('txt-number-1').value;
    var num2El = document.getElementById('txt-number-2').value;
    var num3El = document.getElementById('txt-number-3').value;
    var num4El = document.getElementById('txt-number-4').value;
    var num5El = document.getElementById('txt-number-5').value;
    
    var averageSummary = (num1El*1+num2El*1+num3El*1+num4El*1+num5El*1)/5;
    document.getElementById('average-5-number').innerHTML = averageSummary;

}

function convertUSDtoVND(){
    var rateEl = document.getElementById('txt-usd').value;
    var usdnumberEl = document.getElementById('txt-usd-number').value;

    var summaryUSDEl = rateEl*usdnumberEl;
    document.getElementById('tong-tien-viet').innerHTML = summaryUSDEl;

}

function calculateSP(){
    var lengthEl = document.getElementById('txt-length').value;
    var widthEl = document.getElementById('txt-width').value;
   
    var svalueEl = lengthEl*widthEl;
    var pvalueEl = lengthEl*1+widthEl*1;
    document.getElementById('svalue').innerHTML = svalueEl;
    document.getElementById('pvalue').innerHTML = pvalueEl;

}

function calculateSummaryTwoNumber(){
    var numberTwoNumberCharacterEl = document.getElementById('txt-number-2numbercharacter').value;
    var firtNumberEl = Math.floor(numberTwoNumberCharacterEl / 10);
    var secondNumberEl = numberTwoNumberCharacterEl % 10;
   
    var sumTwoNumberEl = firtNumberEl+secondNumberEl;
    
    document.getElementById('sum-two-number').innerHTML = "Tổng của 2 số "+firtNumberEl + " và "+ secondNumberEl + " là: "+sumTwoNumberEl;    

}
